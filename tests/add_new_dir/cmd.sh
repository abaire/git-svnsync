#!/bin/sh -e

mkdir target
echo >target/some_new_file <<EOF
some content in the subdirectory

blah blah blah
EOF

case "$1" in
svn)
	svn add target
	;;

git)
	git add target
	;;
esac

