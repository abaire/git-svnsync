#!/bin/sh -e

make_file()
{
	echo "some new content" > target/some_new_file
}

case "$1" in
svn)
	svn copy orig target
	svn rm target/some_file
	make_file
	svn add target/some_new_file
	;;

git)
	cp -a orig target
	rm target/some_file
	make_file
	git add target
	;;
esac

