#!/bin/sh -e

make_link() {
	ln -s some/orig/file target
}

case "$1" in
svn)
	#FIXME: not supported by the svn client
	exit 1

	svn rm target
	make_link
	svn add target
	;;

git)
	git rm -r target
	make_link
	git add target
	;;
esac

