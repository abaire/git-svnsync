#!/bin/sh -e

make_file() {
	echo >target <<EOF
#!/bin/sh
echo bar
EOF
	chmod 755 target
}

case "$1" in
svn)
	rm target
	svn propdel svn:special target
	make_file
	svn propset svn:executable * target
	;;

git)
	rm target
	make_file
	git add target
	;;
esac

