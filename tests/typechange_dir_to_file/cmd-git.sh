#!/bin/sh -e

make_file() {
	echo "blah blah blah" >target
}

case "$1" in
svn)
	#FIXME: not supported by the svn client
	exit 1

	svn rm target
	make_file
	svn add target
	;;

git)
	git rm -r target
	make_file
	git add target
	;;
esac

