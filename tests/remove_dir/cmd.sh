#!/bin/sh -e

case "$1" in
svn)
	svn rm target
	;;

git)
	git rm -r target
	;;
esac

