#!/bin/sh -e

chmod 755 target

case "$1" in
svn)
	# subversion does not do it automatically
	svn propset svn:executable '*' target
	;;

git)
	git add target
	;;
esac

