#!/bin/sh -e

case "$1" in
svn)
	svn copy orig target
	;;

git)
	cp -a orig target
	git add target
	;;
esac

