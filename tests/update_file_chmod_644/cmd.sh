#!/bin/sh -e

chmod 644 target

case "$1" in
svn)
	# subversion does not do it automatically
	svn propdel svn:executable target
	;;

git)
	git add target
	;;
esac

