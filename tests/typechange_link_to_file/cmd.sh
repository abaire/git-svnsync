#!/bin/sh -e

make_file() {
	echo "blah blah blah" >target
}

case "$1" in
svn)
	rm target
	svn propdel svn:special target
	make_file
	;;

git)
	rm target
	make_file
	git add target
	;;
esac

