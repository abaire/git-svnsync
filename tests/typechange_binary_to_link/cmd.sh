#!/bin/sh -e

make_link() {
	ln -s some/orig/file target
}

case "$1" in
svn)
	svn rm target
	make_link
	svn add target
	;;

git)
	rm target
	make_link
	git add target
	;;
esac

