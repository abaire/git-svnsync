#!/usr/bin/env python2

from setuptools import setup
import sys

try:
    import svn
except ImportError:
    sys.stderr.write("WARNING: subversion bindings (svn module) are not available\n")



setup(	name		= 'git-svnsync',
	version		= '0.1.2',
	description	= 'Server-side git<->svn synchronisation',
	author		= 'Anthony Baire',
	author_email	= 'Anthony.Baire@irisa.fr',
        url		= 'http://git-svnsync.gforge.inria.fr/',
	scripts		= ["git-svnsync"],

        install_requires = ["lockfile>=0.8", "Magic-file-extensions>=0.2"],

	long_description = '''\
Git-svnsync is a bi-directional server-side synchronisation tool between a git
and a subversion repository.

It is based on hooks and it is designed to allow a smooth transition of
projects from a subversion repository to a git repository. Git-svnsync
guarantees that any branch update ('svn commit' or 'git push') is applied
atomically in both repositories, thus providing a seamless experience to the
developers.
''',

)
