#!/bin/bash -e

#set -x

if [ ! -d tests ] || [ ! -f run_stress_tests.sh ] ; then
	die "must be run from the same dir"
fi

ARCHIVE="`pwd`/test.tar.gz"
GIT_SVNSYNC="`pwd`/git-svnsync"
TMP=tmp-svnsync/stress

rm -rf tmp-svnsync/stress

if [ -e "$TMP" ] ; then
	echo "error: $TMP exists"
	exit 1
fi
mkdir -p "$TMP"
cd "$TMP"


tar zxf "$ARCHIVE"

set -x
(cd test.git && "$GIT_SVNSYNC" init ../test.svn --trunk=trunk)

svnurl="file://`pwd`/test.svn/trunk"
giturl="`pwd`/test.git"

svn co "$svnurl" svncopy
git clone "$giturl" gitcopy

(cd svncopy && touch stress && svn add stress && svn commit -m "add stress file")


svnproc()
{
(
	name="svn$1"
	nb=$2
	pfx=$3
	echo "svnproc $name"
	
	svn co "$svnurl" "$name"
	cd "$name"
	

	if [ -z "$pfx" ] ;
	then
		target=stress
	else
		target="$name-stress"
		touch "$target"
		svn add "$target"
	fi

	i=0
	while i=`expr $i + 1` ; [ $i -le $nb ]
	do
		tag="$name-$i"
		echo "$tag" >> "$target"
		while svn commit -m "$tag" ; [ $? -ne 0 ]
		do
			svn update --accept postpone
			sed '/[<=>]/d' -i "$target"
			svn resolved "$target"
		done
	done
) >"svn$1.log" 2>&1 & 
}

gitproc()
{
(
	name="git$1"
	nb=$2
	pfx=$3
	echo "gitproc $name"
	
	set -x
	git clone "$giturl" "$name"
	cd "$name"
	

	if [ -z "$pfx" ] ;
	then
		target=stress
	else
		target="$name-stress"
	fi

	i=0
	while i=`expr $i + 1` ; [ $i -le $nb ]
	do
		tag="$name-$i"
		echo "$tag" >> "$target"
		git add "$target"
		git commit -m "$tag"
		while git push origin master ; [ $? -ne 0 ]
		do
			git pull --no-commit --rebase
#			while 
#				git pull --rebase
#				sed '/[<=>]/d' -i "$target"
#				git add "$target"
#				git commit -m "merge for $tag"; [ $? -ne 0 ]
#			do
#				echo retry $tag
#			done
		done
	done
) >"git$1.log" 2>&1 & 
}


for i in {1..4}
do
	svnproc $i 10 -
	gitproc $i 10 -
done

wait
(cd svncopy && svn update)
(cd gitcopy && git pull)

(cd svncopy && svn log)|egrep '^ *(git|svn)[0-9]+-[0-9]' | sed 's/^ *//' > svnlog
(cd test.git && git log --format='%B')|egrep '^((git|svn)[0-9]|[*] commit)' > gitlog

echo
diff -u svnlog gitlog
if cmp svnlog gitlog ; then
	echo Success
else
	echo 'Failure: git & svn repositories have different history'
	exit 1
fi

