#!/bin/bash -e

#set -x

die()
{
	echo "$0: fatal: $@"
	exit 1
}

header() {
	echo
	echo "######################################################"
	echo "## $*"
	echo "######################################################"
	echo
}


if [ ! -d tests ] || [ ! -f run_translation_tests.sh ] ; then
	die "must be run from the same dir"
fi

TESTS="`pwd`/tests"
GIT_SVNSYNC="`pwd`/git-svnsync"
TMP=tmp-svnsync/translation

#PYTHON_COVERAGE="python2.7 `which python-coverage` run --append --branch --parallel-mode"
PYTHON_COVERAGE=

mkdir -p "$TMP"
cd "$TMP"

rm -rf {s2g,g2s}_{svn,git}{repo,copy} htmlcov
rm -f .coverage .coverage.*

########################################################
# common functions

install_test_tree_into () {
	test -n "$1"
	(cd "$TESTS" && tar c --exclude .svn .) | (cd "$1" && tar xv)
	(cd "$1" && xargs mkdir < .empty_dirs)
}

init_repos() {
	PFX="$1"

	# create repos
	git init --bare ${PFX}_gitrepo
	svnadmin create ${PFX}_svnrepo

	git_repo="`pwd`/${PFX}_gitrepo"
	svn_repo="`pwd`/${PFX}_svnrepo"

	svn_uuid="`svnlook uuid ${PFX}_svnrepo`"

	# extract working copy
	git clone "$git_repo" ${PFX}_gitcopy
	svn co "file://$svn_repo" ${PFX}_svncopy


	# make the first commit
	install_test_tree_into ${PFX}_gitcopy
	install_test_tree_into ${PFX}_svncopy

	(cd ${PFX}_svncopy && svn add * .gitignore && svn commit -m "svn first commit")
	(cd ${PFX}_gitcopy && git add * .gitignore && git commit -F - <<EOF
git first commit

git-svn-id: file://$svn_repo@1 $svn_uuid
EOF
		git push -u origin master
	)

	(cd ${PFX}_gitrepo && $PYTHON_COVERAGE "$GIT_SVNSYNC" init "$svn_repo")
	
	# patch the hook files to get code coverage
	pycov_quoted="`echo "$PYTHON_COVERAGE" | sed 's/\//\\\\&/g'`"
	for file in \
		${PFX}_gitrepo/hooks/update \
		${PFX}_svnrepo/hooks/pre-commit \
		${PFX}_svnrepo/hooks/post-commit
	do
		sed -i "s/^exec/exec $pycov_quoted/" "$file"
	done
}

create_git_repo_in_svn_copy() {
	(cd "$1" && git init && git add . && git commit -m "svn variant")
}

get_head_tree() {
	(cd "$1" && git rev-parse HEAD^{tree})
}

ls_head_tree() {
	(cd "$1" && git ls-tree -r HEAD)
}

diff_head_trees() {
	ls_head_tree $1 > "tree.$1"
	ls_head_tree $2 > "tree.$2"
	diff -u0 "tree.$1" "tree.$2" || true
}

compare_trees() {
	tree1="$1"
	tree2="$2"
	if [ "`get_head_tree $tree1`" == "`get_head_tree $tree2`" ] ; then
		echo "$tree1 and $tree2 are identical"
		return 0
	else
		diff_head_trees "$tree1" "$tree2"
		echo
		echo "$tree1 and $tree2 differ"
		return 1
	fi
}

########################################################
header "SVN->GIT translation tests"

# init
init_repos s2g

# apply the changes
(cd s2g_svncopy && (

	for dir in * ; do
		(cd "$dir" && 
			echo "s2g_svncopy/$dir"
			[ ! -f "cmd.sh" ]     || ./cmd.sh svn
			[ ! -f "cmd-svn.sh" ] || ./cmd-svn.sh svn
		)
	done

	svn commit -m "svn->git translation tests"
))

# sync the git repo
(cd s2g_gitrepo && $PYTHON_COVERAGE "$GIT_SVNSYNC" sync)
(cd s2g_gitcopy && git pull)

create_git_repo_in_svn_copy s2g_svncopy

########################################################
header "GIT->SVN translation tests"

# init
init_repos g2s

# apply the changes
(cd g2s_gitcopy && (
	for dir in * ; do
		(cd "$dir" && 
			echo "g2s_gitcopy/$dir"
			[ ! -f "cmd.sh" ]     || ./cmd.sh git
			[ ! -f "cmd-git.sh" ] || ./cmd-git.sh git
		)
	done

	git commit -m "git->svn translation tests"
	git push
))

# sync the git repo
(cd g2s_svncopy && svn update)

create_git_repo_in_svn_copy g2s_svncopy

########################################################
header "SVN->GIT translation results"
s2g=true
compare_trees s2g_svncopy s2g_gitcopy || s2g=false

header "GIT->SVN translation results"
g2s=true
compare_trees g2s_gitcopy g2s_svncopy || g2s=false

header "comparison of the final trees"
f2f=true
compare_trees s2g_gitcopy g2s_svncopy || f2f=false

if [ -n "$PYTHON_COVERAGE" ] ; then
	header "Coverage"

	# retrieve coverage files
	mv {s2g,g2s}_*/.coverage.* .

	python-coverage combine
	python-coverage report --include "$GIT_SVNSYNC"
	python-coverage html --include "$GIT_SVNSYNC"
	echo 
	echo " -> file://`pwd`/htmlcov/index.html"
fi

if `$s2g` && `$g2s` && `$f2f` ; then
	header "-> Success" ; exit 0
else
	header "-> Failure" ; exit 1
fi


