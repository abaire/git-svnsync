git-svnsync ‒ Server-side GIT↔SVN synchronisation
=================================================

Git-svnsync is a bi-directional server-side synchronisation tool between a git and a subversion repository.

It is based on hooks and it is designed to allow a smooth transition of projects from a subversion repository to a git repository. Git-svnsync guarantees that any branch update ('svn commit' or 'git push') is applied atomically in both repositories, thus providing a seamless experience to the developers.

## Licence

General Public License v2 (or later)

## Project page

https://gitlab.inria.fr/abaire/git-svnsync

## Requirements
- python 2.7
- [python-lockfile](http://pypi.python.org/pypi/lockfile)
- [python-magic](http://www.darwinsys.com/file/)
- [python-subversion](http://subversion.apache.org/)
- git


## Usage

1. Make sure **git-svnsync** is in the PATH.
2. You have an existing SVN repository to be migrated and a GIT bare repository whose content was filled with **git-svn**, both must be located on the same host.
3. From the git repository, run: **git svnsync init _SVNURL_** ; this will install hooks for git (_update_) and for svn (_pre-commit_ & _post-commit_). If any of these hooks is already present, then the new hooks are installed as _HOOKNAME.git-svnsync_ and you will have to merge them manually.
4. Your setup is ready.


## Caveats

- Both server repositories (svn and git) must be located on the same host
- Both repositories must have the same access permissions (at system level)
- For the moment only one branch can be synchronised (by default the _master_ branch in git and the _trunk_ branch in svn). You can choose different branches with the ``--trunk`` and ``--branch`` options of `git init`.
- For each push on the git side, the tool creates only one commit on the svn side, even if multiple commits are pushed. The detailed history is only available on the git side.
- It is not possible to update both repositories in an atomic way. To overcome this issue, git-svnsync always makes the svn commit first and blocks all git pushes until all incoming svn commits are replicated. If this process is interrupted (power outage, or whatever reason) before the replication is completed you may be left with a git repository that blocks all pushes. Do not panic. You can trigger a new synchronisation either by making a new svn commit or by running **git svnsync sync** from the git repository.
- Apart from the initial import, git-svnsync does not interoperate with git-svn. Whereas git-svn stores its metadata in commit messages, git-svnsync stores them in git notes (refs/notes/svn). This design choice was necessary to allow syncing in both directions.  
